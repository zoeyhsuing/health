package com.itheima.health.dao;

import com.github.pagehelper.Page;
import com.itheima.health.pojo.CheckItem;
import com.itheima.health.pojo.Member;
import org.apache.ibatis.annotations.Param;

import java.util.Date;

public interface MemberDao {


    Member findMemberByTelephone(String telephone);

    void add(Member member);

    Integer findMemberCountsByRegTime(String regTime);

    Integer findTodayNewMember(String today);

    Integer findTotalMember();

    Integer findThisNewMember(String date);

    Integer findManCountBySex();
    Integer findWomanCountBySex();

    Integer findCount1ByAge(Date calendar);
    Integer findCount2ByAge(@Param("endYear") Date calendar1, @Param("beginYear")Date calendar2);
    Integer findCount3ByAge(@Param("endYear")Date calendar2,@Param("beginYear")Date calendar3);
    Integer findCount4ByAge(Date calendar3);

    Page<CheckItem> findPage(String queryString);
}
