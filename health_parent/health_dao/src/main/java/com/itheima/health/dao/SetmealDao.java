package com.itheima.health.dao;

import com.github.pagehelper.Page;
import com.itheima.health.pojo.Setmeal;

import java.util.List;
import java.util.Map;

public interface SetmealDao {

    void add(Setmeal setmeal);

    void addSetmealAndCheckGroup(Map<String, Integer> map);

    Page<Setmeal> findPage(String queryString);

    List<Setmeal> findAll();

    Setmeal findById(Integer id);

    List<Map> findSetmealOrderCount();

    // 查询是否关联
    Integer queryCountById(Integer id);

    // 编辑回显复选框
    List<Integer> findChecked(Integer id);

    // 确认删除
    void delete(Integer id);

    // 编辑保存
    void edit(Setmeal setMeal);

    void deleteAssociation(Integer id);
}
