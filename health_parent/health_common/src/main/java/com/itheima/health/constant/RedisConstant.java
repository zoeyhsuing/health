package com.itheima.health.constant;

/**
 * Redis图片上传存放的常量类
 */
public class RedisConstant {
    public static final String SETMEAL_PIC_RESOURCES = "setmealPicResources";
    public static final String SETMEAL_PIC_DB_RESOURCES = "setmealPicDbResources";
    //根据id查询的套餐信息
    public static final String SETMEAL_SETMEAL_FINDBYID_RESOURCES = "setmealFindbyid";
    //查询所有套餐信息
    public static final String SETMEAL_SETMEAL_ALL_RESOURCES = "setmealAll";
}
