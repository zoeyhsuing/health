package com.itheima.health.service.impl;

import com.alibaba.dubbo.config.annotation.Service;
import com.github.pagehelper.Page;
import com.github.pagehelper.PageHelper;
import com.google.gson.Gson;
import com.itheima.health.constant.RedisConstant;
import com.itheima.health.dao.CheckGroupDao;
import com.itheima.health.dao.CheckItemDao;
import com.itheima.health.dao.MobileDao;
import com.itheima.health.dao.SetmealDao;
import com.itheima.health.entity.PageResult;
import com.itheima.health.pojo.CheckGroup;
import com.itheima.health.pojo.CheckItem;
import com.itheima.health.pojo.Setmeal;
import com.itheima.health.service.SetmealService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.transaction.annotation.Transactional;
import redis.clients.jedis.JedisPool;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * @ClassName CheckItemServiceImpl
 * @Description TODO
 * @Author ly
 * @Company 深圳黑马程序员
 * @Date 2019/10/23 15:57
 * @Version V1.0
 */
@Service(interfaceClass = SetmealService.class)
@Transactional
public class SetmealServiceImpl implements SetmealService {

    @Autowired
    SetmealDao setmealDao;
    @Autowired
    MobileDao mobileDao;
    @Autowired
    JedisPool jedisPool;
    @Autowired
    Gson gson;

    //新增、编辑后 保存到Redis
    private void savePic_IntoRedis(String pic){
        //提交后的
        jedisPool.getResource().sadd(RedisConstant.SETMEAL_PIC_DB_RESOURCES,pic);
    }

    // 新增套餐
    @Override
    public void add(Setmeal setmeal, List<Integer> checkgroupList) {
        // 1：新增套餐，向套餐表中添加1条数据
        setmealDao.add(setmeal);
        /*调用手机端mobileDao的findAll(),Gson转换添加到redis*/
        List<Setmeal> mobileDaoAll = mobileDao.findAll();
        String toJson = gson.toJson(mobileDaoAll);
        jedisPool.getResource().set(RedisConstant.SETMEAL_SETMEAL_ALL_RESOURCES,toJson);
        // 2：新增套餐和检查组的中间表，想套餐和检查组的中间表中插入多条数据
        if(checkgroupList!=null && checkgroupList.size() >0){
            setSetmealAndCheckGroup(setmeal.getId(),checkgroupList);
        }
        // 3：向Redis中集合的key值为setmealPicDbResources下保存数据，数据为图片的名称
        jedisPool.getResource().sadd(RedisConstant.SETMEAL_PIC_DB_RESOURCES,setmeal.getImg());
    }

    // 插入中间表
    private void setSetmealAndCheckGroup(Integer setmealId, List<Integer> checkgroupList) {
        for (Integer checkGroupId : checkgroupList) {
            Map<String,Integer> map = new HashMap<>();
            map.put("setmealId",setmealId);
            map.put("checkGroupId",checkGroupId);
            setmealDao.addSetmealAndCheckGroup(map);
        }
    }

    //分页查询
    @Override
    public PageResult pageQuery(Integer currentPage, Integer pageSize, String queryString) {
        // 初始化分页参数
        PageHelper.startPage(currentPage,pageSize);
        // 查询
        Page<Setmeal> page = setmealDao.findPage(queryString);
        // 组织响应数据
        return new PageResult(page.getTotal(),page.getResult());
    }

    // 移动端显示套餐列表
    @Override
    public List<Setmeal> findAll() {
        return setmealDao.findAll();
    }

    // 编辑查询回显
    @Override
    public Setmeal findById(Integer id) {
        return setmealDao.findById(id);
    }

    // 编辑套餐复选框回显
    @Override
    public List<Integer> findChecked(Integer id) {
        return setmealDao.findChecked(id);
    }

    // 编辑保存
    @Override
    public void edit(Setmeal setMeal, List<Integer> integerList) {
        //保存编辑信息
        setmealDao.edit(setMeal);
        /*更新redis里对应的套餐信息*/
        Setmeal setmeal1 = mobileDao.findById(setMeal.getId());
        String toJson1 = gson.toJson(setmeal1);
        jedisPool.getResource().set(RedisConstant.SETMEAL_SETMEAL_FINDBYID_RESOURCES+setmeal1.getId(),toJson1);

        /*更新redis里的套餐列表信息*/
        List<Setmeal> mobileDaoAll = mobileDao.findAll();
        String toJson = gson.toJson(mobileDaoAll);
        jedisPool.getResource().set(RedisConstant.SETMEAL_SETMEAL_ALL_RESOURCES,toJson);
        jedisPool.getResource().del(RedisConstant.SETMEAL_SETMEAL_FINDBYID_RESOURCES+setmeal1.getId());
        //删除旧的外键表信息
        setmealDao.deleteAssociation(setMeal.getId());

        //保存新的外键表信息
        if (integerList != null && integerList.size() > 0) {
            setSetmealAndCheckGroup(setMeal.getId(),integerList);
        }
        //保存新的图片信息到Redis
        savePic_IntoRedis(setMeal.getImg());
    }

    @Override
    public List<Map> findSetmealOrderCount() {
        return setmealDao.findSetmealOrderCount();
    }

    //删除
    @Override
    public void delete(Integer id) {
        //先查询检查项是否有检查组关联
        if (setmealDao.queryCountById(id) > 0) {
            throw new RuntimeException("该检查项与检查组存在关联，不能删除");
        }
        setmealDao.delete(id);
        /*调用手机端mobileDao的findAll(),Gson转换添加到redis*/
        List<Setmeal> mobileDaoAll = mobileDao.findAll();
        String toJson = gson.toJson(mobileDaoAll);
        jedisPool.getResource().set(RedisConstant.SETMEAL_SETMEAL_ALL_RESOURCES,toJson);
    }

}
