package com.itheima.health.service.impl;

import com.alibaba.dubbo.config.annotation.Service;
import com.github.pagehelper.Page;
import com.github.pagehelper.PageHelper;
import com.itheima.health.dao.MemberDao;
import com.itheima.health.entity.PageResult;
import com.itheima.health.pojo.CheckItem;
import com.itheima.health.pojo.Member;
import com.itheima.health.service.MemberService;
import com.itheima.health.utils.MD5Utils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.transaction.annotation.Transactional;

import java.util.*;

/**
 * @ClassName CheckItemServiceImpl
 * @Description TODO
 * @Author ly
 * @Company 深圳黑马程序员
 * @Date 2019/10/23 15:57
 * @Version V1.0
 */
@Service(interfaceClass = MemberService.class)
@Transactional
public class MemberServiceImpl implements MemberService {

    // 会员
    @Autowired
    MemberDao memberDao;

    @Override
    public Member findMemberByTelephone(String telephone) {
        return memberDao.findMemberByTelephone(telephone);
    }

    @Override
    public void addMember(Member member) {
        // 新增会员的时候，对密码进行加密
        if(member!=null && member.getPassword()!=null){
            member.setPassword(MD5Utils.md5(member.getPassword())); // 密文保存到数据库，保证安全
        }
        memberDao.add(member);
    }

    @Override
    public List<Integer> findMemberCountsByRegTime(List<String> months) {
        // 组织数据
        List<Integer> memberCounts = new ArrayList<>();
        // 格式：[2018-12, 2019-01, 2019-02, 2019-03, 2019-04, 2019-05, 2019-06, 2019-07, 2019-08, 2019-09, 2019-10, 2019-11]
        for (String month : months) {
            String regTime = month+"-32";
            Integer counts = memberDao.findMemberCountsByRegTime(regTime);
            memberCounts.add(counts);
        }
        return memberCounts;
    }

    @Override
    public PageResult pageQuery(Integer currentPage, Integer pageSize, String queryString) {
        // 1：完成对分页初始化工作
        PageHelper.startPage(currentPage,pageSize);
        // 2：查询
        Page<CheckItem> page = memberDao.findPage(queryString);
        // 组织PageResult
        return new PageResult(page.getTotal(),page.getResult());
    }

    @Override
    // 根据性别统计会员数量
    public List<Map<String, Object>> findMemberCountBySex() {

        List<Map<String,Object>> count = new ArrayList<>();

        Map<String,Object> man2 = new HashMap<>();
        Integer man = memberDao.findManCountBySex();
        man2.put("value",man);
        man2.put("name","男");
        count.add(man2);

        Map<String,Object> woman2 = new HashMap<>();
        Integer woman = memberDao.findWomanCountBySex();
        woman2.put("value",woman);
        woman2.put("name","女");
        count.add(woman2);

        return count;
    }

    @Override
    // 根据年龄段统计会员数量
    public List<Map<String, Object>> getMemberByAgeReport() {

        List<Map<String,Object>> count = new ArrayList<>();

        Map<String,Object> countMap1 = new HashMap<>();
        Calendar calendar1 = Calendar.getInstance();
        calendar1.add(Calendar.YEAR,-18);
        Integer count1 = memberDao.findCount1ByAge(calendar1.getTime());
        countMap1.put("value",count1);
        countMap1.put("name","0-18岁");
        count.add(countMap1);

        Map<String,Object> countMap2 = new HashMap<>();
        Calendar calendar2 = Calendar.getInstance();
        calendar2.add(Calendar.YEAR,-30);
        Integer count2 = memberDao.findCount2ByAge(calendar1.getTime(),calendar2.getTime());
        countMap2.put("value",count2);
        countMap2.put("name","18-30岁");
        count.add(countMap2);

        Map<String,Object> countMap3 = new HashMap<>();
        Calendar calendar3 = Calendar.getInstance();
        calendar3.add(Calendar.YEAR,-45);
        Integer count3 = memberDao.findCount3ByAge(calendar2.getTime(),calendar3.getTime());
        countMap3.put("value",count3);
        countMap3.put("name","30-45岁");
        count.add(countMap3);

        Map<String,Object> countMap4 = new HashMap<>();
        Integer count4 = memberDao.findCount4ByAge(calendar3.getTime());
        countMap4.put("value",count4);
        countMap4.put("name","45岁以上");
        count.add(countMap4);

        return count;
    }
}
