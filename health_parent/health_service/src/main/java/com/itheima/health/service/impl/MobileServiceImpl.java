package com.itheima.health.service.impl;

import com.alibaba.dubbo.config.annotation.Service;
import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;
import com.itheima.health.constant.RedisConstant;
import com.itheima.health.dao.MobileDao;
import com.itheima.health.pojo.Setmeal;
import com.itheima.health.service.MobileService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.transaction.annotation.Transactional;
import redis.clients.jedis.Jedis;
import redis.clients.jedis.JedisPool;

import java.util.List;

@Service(interfaceClass = MobileService.class)
@Transactional
public class MobileServiceImpl implements MobileService {

    @Autowired
    MobileDao mobileDao;
    @Autowired
    private JedisPool jedisPool;
    @Autowired
    private Gson gson;


    //查询所有套餐信息
    @Override
    public List<Setmeal> findAll() {
        Jedis jedis = jedisPool.getResource();
        //查询redis中是否存在
        String setmealJson = jedis.get(RedisConstant.SETMEAL_SETMEAL_ALL_RESOURCES);
        if (null != setmealJson&&!setmealJson.equals("")) {
            //将json转成成List对象
            List<Setmeal> setmeallist = gson.fromJson(setmealJson, new TypeToken<List<Setmeal>>() {}.getType());
            return setmeallist;
        }
        //查询数据库
        List<Setmeal> setmeals = mobileDao.findAll();
        String toJson = gson.toJson(setmeals);
        jedisPool.getResource().set(RedisConstant.SETMEAL_SETMEAL_ALL_RESOURCES,toJson);
        return setmeals;
    }

    //根据id查询套餐信息
    public Setmeal findById(Integer id) {
        //先查询redis中是否存在
        Jedis jedis = jedisPool.getResource();
        String setmealJson = jedis.get(RedisConstant.SETMEAL_SETMEAL_FINDBYID_RESOURCES+id);
        if (null != setmealJson&&!setmealJson.equals("")) {
            //将json转换成setmeal对象
            Setmeal setmeal = gson.fromJson(setmealJson, Setmeal.class);
            return setmeal;
        }
        //查询数据库获取套餐数据
        //每个id对应每个套餐
        /*更新redis里对应的套餐信息*/
        Setmeal setmeal1= mobileDao.findById(id);
        String toJson1 = gson.toJson(setmeal1);
        jedisPool.getResource().set(RedisConstant.SETMEAL_SETMEAL_FINDBYID_RESOURCES+setmeal1.getId(),toJson1);
        return setmeal1;
    }

}