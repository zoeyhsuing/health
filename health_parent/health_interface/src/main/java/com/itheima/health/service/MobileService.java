package com.itheima.health.service;

import com.itheima.health.pojo.Setmeal;

import java.util.List;

public interface MobileService {

    List<Setmeal> findAll();
    Setmeal findById(Integer id);

}
