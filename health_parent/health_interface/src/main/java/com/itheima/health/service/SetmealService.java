package com.itheima.health.service;

import com.itheima.health.entity.PageResult;
import com.itheima.health.pojo.Setmeal;

import java.util.List;
import java.util.Map;

public interface SetmealService {


    void add(Setmeal setmeal, List<Integer> checkgroupList);

    PageResult pageQuery(Integer currentPage, Integer pageSize, String queryString);

    List<Setmeal> findAll();

    Setmeal findById(Integer id);

    List<Map> findSetmealOrderCount();

    void delete(Integer id);

    List<Integer> findChecked(Integer id);

    void edit(Setmeal setMeal, List<Integer> integerList);
}
