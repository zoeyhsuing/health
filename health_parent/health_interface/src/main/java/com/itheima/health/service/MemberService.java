package com.itheima.health.service;

import com.itheima.health.entity.PageResult;
import com.itheima.health.pojo.Member;

import java.util.List;
import java.util.Map;

public interface MemberService {


    Member findMemberByTelephone(String telephone);

    void addMember(Member member);

    List<Integer> findMemberCountsByRegTime(List<String> months);

    List<Map<String, Object>> findMemberCountBySex();

    List<Map<String, Object>> getMemberByAgeReport();

    PageResult pageQuery(Integer currentPage, Integer pageSize, String queryString);
}
